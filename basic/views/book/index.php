<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models;
use app\models\Book;
use app\models\BookSearch;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'book_name',
            [
                'attribute' => 'book_image',
                'format' => 'html',
                'label' => 'Book_image',
                'value' => function ($data) {
                    return Html::img(Yii::$app->request->BaseUrl.'/uploads/' . $data->book_image,
                        ['width' => '60px']);
                },
            ],
            'book_year',
            [
                    'class' => 'yii\grid\DataColumn',
                    'value' => function($data){
                        $book = new Book($data);
//                        return $data->getNameBookStatus($data->book_status);
//                        return $data->getNameBookStatus();
                        //return $book->getNameBookStatus($book->book_status);
                    }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'download' => function ($url, $model, $key) {
                        return Html::a ( '<span class="glyphicon glyphicon-download"></span> ', ['book/download', 'id' => $model->id] );
                    },
                ],
                'template' => '{update} {view} {delete} {download}'
            ],
        ],
    ]); ?>
</div>
