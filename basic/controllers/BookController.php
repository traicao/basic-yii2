<?php

namespace app\controllers;

use Yii;
use app\models\Book;
use app\models\BookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['?'],
                        'matchCallback' =>$this->allowPermission(),
                        'denyCallback' => function ($rule, $action){return $this->goHome();},

                    ],

                ],
            ],
        ];
    }


    public function allowPermission()
    {
        if(isset($_GET["id"])) {
            $model = $this->findModel($_GET["id"]);
           // var_dump(Yii::$app->user->isGuest && $model->book_status != 0);die;
            if (Yii::$app->user->isGuest && $model->book_status != 0) {
                return false;
            }
            if (!Yii::$app->user->isGuest) {
                // $model = $this->findModel($_GET["id"]);
                //var_dump($model->book_status == 2 && $model->book_user != Yii::$app->user->id);die;
                return !($model->book_status == 2 && $model->book_user != Yii::$app->user->id);
            }
        }
    }
    /**
     * Lists all Book models.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new BookSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //var_dump(Yii::$app->request->queryParams);die;
        $dataProvider->pagination->defaultPageSize=2;
//        $dataProvider->query->andWhere(['book_status' => 0] );
//        if (!Yii::$app->user->isGuest)
//        {
//            $user_id = Yii::$app->user->id;
//            $dataProvider->query->orWhere(['book_status'=> 1]);
//            $dataProvider->query->orWhere(['and',['book_status'=> 2],['book_user'=>$user_id]]);
//
//        }
       // print_r($dataProvider->query);exit;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }


    function actionDownload(){


        $book_id = $_GET['id'] ;
        $model = $this->findModel($book_id);
        $image = $model->book_image;
//        $book_status = $_GET['book_status'] ;
//        if($book_status==2 && $user_id!=Yii::app()->user->id )
//        {
//            return;
//        }

//	    var_dump($name);
        $filecontent=file_get_contents(Yii::getAlias('@webroot').'/uploads/'.$image);
        header("Content-Type: text/plain");
        header("Content-disposition: attachment; filename=$image");
        header("Pragma: no-cache");
        echo $filecontent;
        exit;
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();

        if ($model->load(Yii::$app->request->post())) {
            //var_dump(UploadedFile::getInstance($model, 'book_image'));die;
            $model->book_image = UploadedFile::getInstance($model, 'book_image');
            $model->book_user = Yii::$app->user->id;
           // var_dump($model->book_status);die;
            if ($model->save()) {
                if($model->upload()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else
                {
                    var_dump('bien');die;
                }

            }

        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
