<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $book_name
 * @property string $book_image
 * @property int $book_year
 * @property int $book_status
 * @property int $book_user
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_name', 'book_year','book_status'], 'required'],
            [['book_year'], 'integer'],
            //[['book_name', 'book_image'], 'max' => 250],
            [['book_image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_name' => 'Book Name',
            'book_image' => 'Book Image',
            'book_year' => 'Book Year',
            'book_status' => 'Book Status',
            'book_user' => 'Book User',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
           // var_dump(Yii::getAlias('@webroot'));die;
            $this->book_image->saveAs(Yii::getAlias('@webroot').'/uploads/' . $this->book_image->baseName . '.' . $this->book_image->extension);
            return true;
        } else {

            return false;
        }
    }

    public static function changeBookStatus()
    {
        return array(0=>'public',1=>'protected',2=>'private');
    }

    public function getNameBookStatus($status)
    {
        return Books::changeBookStatus()[$status];
        //return $temp[$this->book_status];
    }

}
