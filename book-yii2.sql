/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.7.19-0ubuntu0.16.04.1 : Database - bookyii2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bookyii2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bookyii2`;

/*Table structure for table `books` */

DROP TABLE IF EXISTS `books`;

CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(250) NOT NULL,
  `book_image` varchar(250) DEFAULT NULL,
  `book_year` int(11) NOT NULL,
  `book_status` int(11) NOT NULL,
  `book_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `books` */

insert  into `books`(`id`,`book_name`,`book_image`,`book_year`,`book_status`,`book_user`) values 
(1,'a','abc.jpg',1990,0,1),
(2,'b','abc.jpg',1998,1,2),
(3,'c','abc.jpg',1990,1,1),
(4,'d','abc.jpg',1990,1,1),
(5,'e','abc.jpg',1990,2,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `password_reset_token` varchar(250) DEFAULT NULL,
  `user_image` varchar(250) DEFAULT 'default.jpg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`email`,`authKey`,`password_reset_token`,`user_image`) values 
(1,'crtrai','$2y$13$wdwZ3pP.gdEaukzSqCzoO.eusC4nhQXlZ0xqcDRjpipZB/frZrZ3G','crtrai@gmail.com','TGEazYi9LSDJhQ91syeIzBxInYW-grRW',NULL,'default.jpg'),
(2,'crtrai1','$2y$13$ffAKhJXSHmSAfRTczSJgUuRPDkHW4LvS.ZtOenyC4ojRC8MKmCqN.','crtrai1@gmail.com','Rm2OdtJS1DtzGPhSN9f_6C-0qXlQgKUp',NULL,'default.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
